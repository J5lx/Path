<?php namespace J5lx\Path;

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @file
 * This file contains the helper class for specifying platforms.
 * @author Jakob Gahde <j5lx@fmail.co.uk>
 * @copyright Mozilla Public License 2.0
 */

use MyCLabs\Enum\Enum;

/**
 * This is a helper class serving as an enum for specifying a platform. The
 * easiest way to use it is using the <tt>Platform::CLASS_CONSTANT()</tt> syntax
 * (where CLASS_CONSTANT has to be replaced with one of the class constants,
 * obviously), which automatically creates a new instance of the class
 * representing the respective class constant.
 */
class Platform extends Enum
{
    /**
     * Used for Windows operating systems
     */
    const WINDOWS = 'windows';
    /**
     * Used for unixoid operating systems (including systems like Linux)
     */
    const UNIX = 'unix';
}
