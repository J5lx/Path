<?php namespace J5lx\Path;

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @file
 * This file contains the main class of the library.
 * @author Jakob Gahde <j5lx@fmail.co.uk>
 * @copyright Mozilla Public License 2.0
 */

use InvalidArgumentException;

/**
 * This is the main class of the library. It's functionality is available as
 * static methods. Whenever reasonable, the methods also accept an optional
 * parameter for specifying the platform which's path style should be used.
 */
class Path
{
    /**
     * Remove common parts in the beginning of two paths.
     *
     * @param String          $path1, $path2 The two paths
     * @param Platform | null $platform      The platform which's path style
     *                                       should be used. If this param is
     *                                       omitted or null the platform is
     *                                       autodetected.
     * @return An array with both paths, each with the common parts removed
     */
    public static function removeCommonStart(
        $path1,
        $path2,
        Platform $platform = null
    ) {
        $platform = static::getPlatform($platform);

        $path1 = static::split(static::canonical($path1, $platform), $platform);
        $path2 = static::split(static::canonical($path2, $platform), $platform);

        while (count($path1)
            && count($path2)
            && static::isIdentical($path1[0], $path2[0], $platform)
        ) {
            array_shift($path1);
            array_shift($path2);
        }

        return [
            static::join($path1, $platform),
            static::join($path2, $platform)
        ];
    }

    /**
     * Split a path into it's parts.
     *
     * @param String          $path     The path to split
     * @param Platform | null $platform The platform which's path style should
     *                                  be used. If this param is omitted or
     *                                  null the platform is autodetected.
     * @return An array containing the parts of the path
     * @see join
     */
    public static function split($path, Platform $platform = null)
    {
        $separator = static::getSeparator($platform);

        // Unify the separators
        if ($separator != '/') {
            $path = str_replace($separator, '/', $path);
        }
        $path = trim($path, '/');

        // Split the path on every occurence of one or more slash(es)
        $parts = array_filter(preg_split('#[/]+#', $path), function ($part) {
            return !empty($part);
        });

        return $parts;
    }

    /**
     * Join parts of a path. All directory separators will be normalized (e.g.
     * on Windows `/` is replaced with `\`)
     *
     * @param Array           $parts    The parts to join
     * @param Platform | null $platform The platform which's path style should
     *                                  be used. If this param is omitted or
     *                                  null the platform is autodetected.
     * @return The resulting path
     * @see split
     */
    public static function join(Array $parts, Platform $platform = null)
    {
        $separator = static::getSeparator($platform);

        // Handle absolute paths and Windows network paths
        $prependSeparators = 0;
        reset($parts);
        $part = str_replace($separator, '/', current($parts));
        if (substr($part, 0, 1) == '/') {
            $prependSeparators++;
            if (substr($part, 1, 1) == '/' && $platform == Platform::WINDOWS) {
                $prependSeparators++;
            }
        }

        foreach ($parts as $key => $part) {
            if ($separator != '/') {
                $part = trim($part, $separator);
            }
            $parts[$key] = trim($part, '/');
        }

        // Absolute path and Windows network path handling again
        $parts = array_merge(array_fill(0, $prependSeparators, ''), $parts);

        $path = implode($separator, $parts);

        if ($separator != '/') {
            $path = str_replace('/', $separator, $path);
        }

        return $path;
    }

    /**
     * Canonicalize a path.
     *
     * @param String          $path     The path to canonicalize.
     * @param Platform | null $platform The platform which's path style should
     *                                  be used. If this param is omitted or
     *                                  null the platform is autodetected.
     * @return The resulting path
     */
    public static function canonical($path, Platform $platform = null)
    {
        $separator = static::getSeparator($platform);

        // Unify the separators
        if ($separator != '/') {
            $path = str_replace($separator, '/', $path);
        }

        // Handle absolute paths and Windows network paths
        $prependSeparators = 0;
        if (substr($path, 0, 1) == '/') {
            $prependSeparators++;
            if (substr($path, 1, 1) == '/' && $platform == Platform::WINDOWS) {
                $prependSeparators++;
            }
        }

        $parts           = static::split($path, $platform);
        $resultParts     = [];
        $noActualDirsYet = true;
        foreach ($parts as $part) {
            switch ($part) {
                case '.':
                    break;
                case '..':
                    if (!$noActualDirsYet) {
                        array_pop($resultParts);
                        break;
                    }
                    $resultParts[] = $part;
                    break;
                default:
                    $resultParts[]   = $part;
                    $noActualDirsYet = false;

            }
        }

        // Absolute path and Windows network path handling again
        $resultParts = array_merge(
            array_fill(0, $prependSeparators, ''),
            $resultParts
        );

        $result = static::join($resultParts, $platform);

        return $result;
    }

    /**
     * Check whether two paths are identical. This takes things like the case
     * insensitivity on Windows into account.
     *
     * @param String          $path1,$path2 The paths to compare
     * @param Platform | null $platform     The platform which's path style
     *                                      should be used. If this param is
     *                                      omitted or null the platform is
     *                                      autodetected.
     * @return true if both paths are identical, false if not.
     */
    public static function isIdentical(
        $path1,
        $path2,
        Platform $platform = null
    ) {
        $platform = static::getPlatform($platform);

        if ($platform == Platform::WINDOWS) {
            $path1 = strtolower($path1);
            $path2 = strtolower($path2);
        }

        $path1 = static::canonical($path1, $platform);
        $path2 = static::canonical($path2, $platform);

        return $path1 == $path2;
    }

    /**
     * Check whether a path is absolute.
     *
     * @param String          $path     The path to check.
     * @param Platform | null $platform The platform which's path style should
     *                                  be used. If this param is omitted or
     *                                  null the platform is autodetected.
     * @return true if the path is absolute, false if not
     */
    public static function isAbsolute($path, Platform $platform = null)
    {
        $platform = static::getPlatform($platform);

        if ($platform == Platform::WINDOWS) {
            // Does the path begin with two backslashes (network drive) or a
            // char, a colon, and a backslash (local drive)?
            // Plain regex (without string escapes): ^(([a-zA-Z]:)\\|\\\\)
            return preg_match('/^(([a-zA-Z]:)\\\\|\\\\\\\\)/', $path) == 1;
        }

        return substr($path, 0, 1) == '/';
    }

    /**
     * Get the root of an absolute path (i.e. drive on Windows, '' on Unix
     * (since it has only one global file tree))
     *
     * @param String          $path     The path to check to get the root from.
     * @param Platform | null $platform The platform which's path style should
     *                                  be used. If this param is omitted or
     *                                  null the platform is autodetected.
     * @return The root of the given path
     */
    public static function getRoot($path, Platform $platform = null)
    {
        $platform = static::getPlatform($platform);

        if (!static::isAbsolute($path, $platform)) {
            throw new InvalidArgumentException('$path must be absolute.');
        }

        if ($platform == Platform::UNIX) {
            return '';
        }

        $path = static::canonical($path, $platform);
        // WARNING: Regex Monster™ - Attempts to comprehend all of this
        //          on your own may result in serious damage to your
        //          brain. Assistance of a tool like regex101 is highly
        //          recommended - See https://regex101.com/r/rO9zU6/3!
        $regex = <<<'REGEX'
/^(
    [a-zA-Z]:(?=\\)               (?# drive letter)
    |\\\\(?:                      (?# alt: network path)
        [^\*:<>?\\\/|]+\\         (?# [server]\)
        [^\*:<>?\\\/|]+           (?# [share])
        |\?\\(?:                  (?# alt: variants with a ?)
            [a-zA-Z]:(?=\\)       (?# drive letter)
            |(?:                  (?# alt: network share)
                (?:UNC\\)?        (?# opt: UNC\)
                [^\*:<>?\\\/|]+\\ (?# [server]\)
                [^\*:<>?\\\/|]+   (?# [share])
            )
        )
    )
)/x
REGEX;
        preg_match($regex, $path, $matches);
        return $matches[1];
    }

    /**
     * Make a relative path absolute. If it is already absolute the original
     * path is returned.
     *
     * @note The resulting path is not canonicalized by this method
     *
     * @param String          $path     The path to make absolute.
     * @param String          $parent   The parent path of $path. It's assumed
     *                                  to be a directory and must be absolute.
     * @param Platform | null $platform The platform which's path style should
     *                                  be used. If this param is omitted or
     *                                  null the platform is autodetected.
     * @return The resulting path
     * @throws InvalidArgumentException when $parent isn't absolute
     * @see makeRelative
     */
    public static function makeAbsolute(
        $path,
        $parent,
        Platform $platform = null
    ) {
        $platform = static::getPlatform($platform);

        // if $path is already absolute no further action is needed
        if (static::isAbsolute($path, $platform)) {
            return $path;
        }

        // Obviously we can't make $path absolute to a relative path
        if (!static::isAbsolute($parent, $platform)) {
            throw new InvalidArgumentException('$parent must be absolute.');
        }

        $result = static::join([$parent, $path], $platform);
        return $result;
    }

    /**
     * Make an absolute path relative. If it is already relative the original
     * path is returned.
     *
     * @note The resulting path is not canonicalized by this method
     *
     * @param String          $path       The path to make relative.
     * @param String          $relativeTo The path to make the first path
     *                                    relative to. It's assumed to be a
     *                                    directory and must be absolute.
     * @param Platform | null $platform   The platform which's path style should
     *                                    be used. If this param is omitted or
     *                                    null the platform is autodetected.
     * @return The resulting path
     * @throws InvalidArgumentException when $relativeTo isn't absolute
     * @see makeAbsolute
     */
    public static function makeRelative(
        $path,
        $relativeTo,
        Platform $platform = null
    ) {
        $platform  = static::getPlatform($platform);

        // If $path is relative already no further action is required
        if (!static::isAbsolute($path, $platform)) {
            return $path;
        }

        // If both paths are the same the result is always .
        if (static::isIdentical($path, $relativeTo, $platform)) {
            return '.';
        }

        // Obviously we can't make $path relative to a relative path
        if (!static::isAbsolute($relativeTo, $platform)) {
            throw new InvalidArgumentException('$relativeTo must be absolute.');
        }

        list($path, $relativeTo) = static::removeCommonStart(
            $path,
            $relativeTo,
            $platform
        );

        $parts           = static::split($path, $platform);
        $relativeToParts = static::split($relativeTo, $platform);

        // For every item which is unique to $relativeToParts add a '..' item
        // to $relativeParts
        $relativeParts = array_fill(0, count($relativeToParts), '..');
        // Then append the unique parts of $path
        $relativeParts = array_merge($relativeParts, $parts);

        // And finally build the resulting relative path and return it
        $relativePath = static::join($relativeParts, $platform);
        return $relativePath;
    }

    /**
     * Get the directory separator used on the given platform.
     *
     * @param Platform | null $platform The platform which's path style should
     *                                  be used. If this param is omitted or
     *                                  null the platform is autodetected.
     * @return The separator
     */
    public static function getSeparator(Platform $platform = null)
    {
        $platform = static::getPlatform($platform);

        if ($platform == Platform::WINDOWS) {
            return '\\';
        }
        // Only Windows has the backslash, everything else has a slash atm.
        return '/';
    }

    /**
     * Return the given Platform or autodetect the current one if null is given.
     *
     * @param Platform | null $platform Either a Platform or null. In the latter
     *                                  case the platform will be autodetected,
     *                                  else the given Platform is returned.
     * @return The resulting platform
     */
    public static function getPlatform(Platform $platform = null)
    {
        if (!is_null($platform)) {
            return $platform;
        }

        // The PHP_WINDOWS_VERSION_MAJOR constant is only available on Windows
        // so we can check for it to determine whether we're running on Windows
        if (defined("PHP_WINDOWS_VERSION_MAJOR")) {
            return Platform::WINDOWS();
        }

        return Platform::UNIX();
    }
}
