J5lx\Path
=========

[![Travis](https://img.shields.io/travis/J5lx/Path.svg)](https://travis-ci.org/J5lx/Path)
[![HHVM](https://img.shields.io/hhvm/J5lx/Path.svg)](http://hhvm.h4cc.de/package/J5lx/Path)
[![Code Climate](https://img.shields.io/codeclimate/coverage/github/J5lx/Path.svg)](https://codeclimate.com/github/J5lx/Path)
[![SensioLabs Insight](https://img.shields.io/sensiolabs/i/8a161973-d5d6-4699-a982-dc6720c04108.svg)](https://insight.sensiolabs.com/projects/8a161973-d5d6-4699-a982-dc6720c04108)
[![Code Climate](https://img.shields.io/codeclimate/github/J5lx/Path.svg)](https://codeclimate.com/github/J5lx/Path)
[![Packagist](https://img.shields.io/packagist/v/j5lx/path.svg)](https://packagist.org/packages/j5lx/path)
[![Packagist](https://img.shields.io/packagist/dt/j5lx/path.svg)](https://packagist.org/packages/j5lx/path)
[![Packagist](https://img.shields.io/packagist/l/j5lx/path.svg)](https://packagist.org/packages/j5lx/path)

This is a library for working with paths. However it does not perform any tasks
depending on the (file)system like checking whether a certain path exists or
determining a user's home directory.

Features
--------

- Split paths
- Join paths
- Canonicalize paths
- Check whether paths are absolute (or not)
- Make paths absolute or relative
- Can be used for both Windows and Unix paths (e.g. it doesn't treat `\` as a
  directory separator while handling Unix paths)

Usage
-----

### Standard stuff

```php
<?php

require_once 'vendor/autoload.php';

use J5lx\Path\Path;
use J5lx\Path\Platform;
```

> **Note:** You only need the Platform class if you want to work with Windows
> paths on Unix or the reverse.

### Canonicalize a path

```php
var_export(Path::canonical('path/../to/./directory\\..', Platform::UNIX()));
// => "to/directory\\.."

var_export(Path::canonical('path/../to/./directory\\..', Platform::WINDOWS()));
// => "to"
```

### Check whether a path is absolute

```php
var_export(Path::isAbsolute('dir/file'));
// => false
var_export(Path::isAbsolute('/dir/file'));
// => true
```

> **Note:** Windows paths like `\a\path` (relative to current drive) count as
> relative, even though they are quite absolute compared to paths like `a\path`.

### Get the "root" of an absolute path (like Windows drive)

Supports local paths as well as various types of network paths on Windows.

```php
var_export(Path::getRoot('C:\\a\\path', Platform::WINDOWS()));
// => "C:"
var_export(Path::getRoot('/a/path', Platform::UNIX()));
// => "" (There is only one global file tree on Unix)
```

### Make a path absolute if it is not

```php
var_export(Path::makeAbsolute('dir/file', '/home/user'));
// => "/home/user/dir/file"

var_export(Path::makeAbsolute('/dir/file', '/home/user'));
// => "/dir/file"
```

### Make a path relative if it is not

```php
var_export(Path::makeRelative('/dir/file', '/dir/path/to/other/dir'));
// => "../../../../file"

var_export(Path::makeRelative('dir/file', '/dir/path/to/other/dir'));
// => "dir/file"
```

### Join several paths

```php
var_export(Path::join(['one', 'two', 'three\\../four']));
// *nix => "one/two/three\\../four" // *nix paths can contain backslashes
//  win => "one\\two\\three\\..\\four"
```

### Split a path

```php
var_export(Path::split('one/two/three\\../four'));
// *nix => array (
//           0 => 'one',
//           1 => 'two',
//           2 => 'three\\..', // *nix paths can contain backslashes
//           3 => 'four',
//         )
//  win => array (
//           0 => 'one',
//           1 => 'two',
//           2 => 'three',
//           3 => '..',
//           4 => 'four',
//         )
```

TODO
----

- Maybe better abstraction of the platform-specific stuff

Contributing to this project
----------------------------

Anyone and everyone is welcome to contribute. Please take a moment to
review the [guidelines for contributing](CONTRIBUTING.md).

* [Bug reports](CONTRIBUTING.md#bugs)
* [Feature requests](CONTRIBUTING.md#features)
* [Pull requests](CONTRIBUTING.md#pull-requests)
