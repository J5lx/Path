<?php

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use J5lx\Path\Path;
use J5lx\Path\Platform;

class PathTransformsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers J5lx\Path\Path::removeCommonStart
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::isIdentical
     */
    public function testRemoveCommonStartRemovesCommonStart()
    {
        $paths = Path::removeCommonStart('/path/to/a/file', 'path/to/another/file', Platform::UNIX());
        $this->assertEquals('a/file', $paths[0]);
        $this->assertEquals('another/file', $paths[1]);
    }

    /**
     * @covers J5lx\Path\Path::split
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testSplitDoesntSplitByWindowsSeparatorsOnUnix()
    {
        $parts = Path::split('/path/to/a\\directory', Platform::UNIX());
        $this->assertEquals(['path', 'to', 'a\\directory'], $parts);
    }

    /**
     * @covers J5lx\Path\Path::split
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testSplitSplitsByWindowsAndUnixSeparatorsOnWindows()
    {
        $parts = Path::split('/path/to/a\\directory', Platform::WINDOWS());
        $this->assertEquals(['path', 'to', 'a', 'directory'], $parts);
    }

    /**
     * @covers J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testJoinWithUnixPathsWithBackslashes()
    {
        $path = Path::join(['one', 'two', 'three\\../four'], Platform::UNIX());
        $this->assertEquals('one/two/three\\../four', $path);
    }

    /**
     * @covers J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testJoinWithWindowsPathsWithSlashes()
    {
        $path = Path::join(['one', 'two', 'three\\../four'], Platform::WINDOWS());
        $this->assertEquals('one\\two\\three\\..\\four', $path);
    }

    /**
     * @covers J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testJoinPreservesFirstAbsolutePath()
    {
        $path = Path::join(['/one', 'two'], Platform::UNIX());
        $this->assertEquals('/one/two', $path);
    }

    /**
     * @covers J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testJoinPreservesFirstNetworkPath()
    {
        $path = Path::join(['\\\\server\\share\\one', 'two'], Platform::WINDOWS());
        $this->assertEquals('\\\\server\\share\\one\\two', $path);
    }

    /**
     * @covers J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testCanonicalHandlesAbsolutePaths()
    {
        $path = Path::canonical('/a/../path', Platform::UNIX());
        $this->assertEquals('/path', $path);
    }

    /**
     * @covers J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testCanonicalHandlesNetworkPaths()
    {
        $path = Path::canonical('\\\\server\\share\\a\\..\\path', Platform::WINDOWS());
        $this->assertEquals('\\\\server\\share\\path', $path);
    }

    /**
     * @covers J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testCanonicalDoesntMindWindowsSeparatorsOnUnix()
    {
        $path = Path::canonical('/path/../to/./directory\\..', Platform::UNIX());
        $this->assertEquals('/to/directory\\..', $path);
    }

    /**
     * @covers J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testCanonicalDoesMindWindowsSeparatorsOnWindows()
    {
        $path = Path::canonical('\\path/../to/./directory\\..', Platform::WINDOWS());
        $this->assertEquals('\\to', $path);
    }

    /**
     * @covers J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testCanonicalDoesntStripLeadingParentReferences()
    {
        $path = Path::canonical('../../path');
        $this->assertEquals('../../path', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeAbsolute
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeAbsoluteAppendsRelativePathToParentPathOnUnix()
    {
        $path = Path::makeAbsolute('a/path', '/path/to', Platform::UNIX());
        $this->assertEquals('/path/to/a/path', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeAbsolute
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeAbsoluteReturnsOriginalPathIfAlreadyAbsoluteOnUnix()
    {
        $path = Path::makeAbsolute('/a/path', '/path/to', Platform::UNIX());
        $this->assertEquals('/a/path', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeAbsolute
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeAbsoluteAppendsRelativePathToParentPathOnWindows()
    {
        $path = Path::makeAbsolute('a\\path', 'C:\\path\\to', Platform::WINDOWS());
        $this->assertEquals('C:\\path\\to\\a\\path', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeAbsolute
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeAbsoluteReturnsOriginalPathIfAlreadyAbsoluteOnWindows()
    {
        $path = Path::makeAbsolute('C:\\a\\path', 'C:\\path\\to', Platform::WINDOWS());
        $this->assertEquals('C:\\a\\path', $path);
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage $parent must be absolute.
     */
    public function testMakeThrowsExceptionWhenParentPathIsRelative()
    {
        Path::makeAbsolute('a/path', 'path/to', Platform::UNIX());
    }

    /**
     * @covers J5lx\Path\Path::makeRelative
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::removeCommonStart
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeRelativeWorksOnUnix()
    {
        $path = Path::makeRelative('/path/to/some/file', '/path/to/a/folder', Platform::UNIX());
        $this->assertEquals('../../some/file', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeRelative
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::removeCommonStart
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeRelativeReturnsCurrentDirectoryReferenceWhenPathsAreEqualOnUnix()
    {
        $path = Path::makeRelative('/path/to/a/folder', '/path/to/a/folder', Platform::UNIX());
        $this->assertEquals('.', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeRelative
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::removeCommonStart
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeRelativeRetursOriginalPathIfAlreadyRelativeOnUnix()
    {
        $path = Path::makeRelative('path/to/some/file', '/path/to/a/folder', Platform::UNIX());
        $this->assertEquals('path/to/some/file', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeRelative
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::removeCommonStart
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeRelativeWorksOnWindws()
    {
        $path = Path::makeRelative('C:\\path\\to\\some\\file', 'C:\\Path\\to\\a\\folder', Platform::WINDOWS());
        $this->assertEquals('..\\..\\some\\file', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeRelative
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::removeCommonStart
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeRelativeReturnsOriginalPathIfAlreadyRelativeOnWindows()
    {
        $path = Path::makeRelative('path\\to\\some\\file', 'C:\\Path\\to\\a\\folder', Platform::WINDOWS());
        $this->assertEquals('path\\to\\some\\file', $path);
    }

    /**
     * @covers J5lx\Path\Path::makeRelative
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::removeCommonStart
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::isAbsolute
     */
    public function testMakeRelativeReturnsCurrentDirectoryReferenceWhenPathsAreEqualOnWindows()
    {
        $path = Path::makeRelative('C:\\path\\to\\a\\folder', 'C:\\Path\\to\\a\\folder', Platform::WINDOWS());
        $this->assertEquals('.', $path);
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage $relativeTo must be absolute.
     */
    public function testMakeRelativeThrowsExceptionWhenOtherPathIsRelative()
    {
        Path::makeRelative('/path/to/some/file', 'path/to/a/folder', Platform::UNIX());
    }
}
