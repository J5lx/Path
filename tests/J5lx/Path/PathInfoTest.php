<?php

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use J5lx\Path\Path;
use J5lx\Path\Platform;

class PathInfoTest extends \PHPUnit_Framework_TestCase
{
    protected $phpWinVerMajDefined = false;
    protected $phpWinVerMaj = "";

    protected function canDeleteConstants()
    {
        return extension_loaded('runkit') || extension_loaded('uopz');
    }

    protected function deleteConstant($constant)
    {
        if (!defined($constant)) {
            return true;
        }
        if (extension_loaded('runkit')) {
            runkit_constant_remove('PHP_WINDOWS_VERSION_MAJOR');
            return true;
        }
        if (extension_loaded('uopz')) {
            uopz_undefine('PHP_WINDOWS_VERSION_MAJOR');
            return true;
        }
        return false;
    }

    protected function setUp()
    {
        $this->phpWinVerMajDefined = defined("PHP_WINDOWS_VERSION_MAJOR");
        $this->phpWinVerMaj        = $this->phpWinVerMajDefined ? $this->phpWinVerMaj : "";
    }

    protected function tearDown()
    {
        // We'll reset the PHP_WINDOWS_VERSION_MAJOR constant to it's original
        // state so we don't accidentally break other tests.
        if ($this->phpWinVerMajDefined && !defined("PHP_WINDOWS_VERSION_MAJOR")) {
            define("PHP_WINDOWS_VERSION_MAJOR", $this->phpWinVerMaj);
        }
        if (!$this->phpWinVerMajDefined && defined("PHP_WINDOWS_VERSION_MAJOR")) {
            $this->deleteConstant("PHP_WINDOWS_VERSION_MAJOR");
        }
    }

    /**
     * @covers J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     */
    public function testIsIdenticalRecognizesParentReferences()
    {
        $res = Path::isIdentical('/path/to/file', '/path/to/a/../file', Platform::UNIX());
        $this->assertEquals(true, $res);

        $res = Path::isIdentical('C:\\path\\to\\file', 'C:\\path\\to\\a\\..\\file', Platform::WINDOWS());
        $this->assertEquals(true, $res);
    }

    /**
     * @covers J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     */
    public function testIsIdenticalIsCaseSensitiveOnUnix()
    {
        $res = Path::isIdentical('/path/to/file', '/Path/to/file', Platform::UNIX());
        $this->assertEquals(false, $res);
    }

    /**
     * @covers J5lx\Path\Path::isIdentical
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::canonical
     */
    public function testIsIdenticalIsntCaseSensitiveOnWindows()
    {
        $res = Path::isIdentical('C:\\path\\to\\file', 'C:\\Path\\to\\file', Platform::WINDOWS());
        $this->assertEquals(true, $res);
    }

    /**
     * @covers J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     */
    public function testIsAbsoluteRecognizesAbsolutePathsOnUnix()
    {
        $abs = Path::isAbsolute('/a/path', Platform::UNIX());
        $this->assertEquals(true, $abs);
    }

    /**
     * @covers J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     */
    public function testIsAbsoluteRecognizesRelativePathsOnUnix()
    {
        $abs = Path::isAbsolute('a/path', Platform::UNIX());
        $this->assertEquals(false, $abs);
    }

    /**
     * @covers J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     */
    public function testIsAbsoluteRecognizesAbsolutePathsOnWindows()
    {
        // Local path
        $abs = Path::isAbsolute('C:\\a\\path', Platform::WINDOWS());
        $this->assertEquals(true, $abs);

        // Network path
        $abs = Path::isAbsolute('\\\\a\\path', Platform::WINDOWS());
        $this->assertEquals(true, $abs);
    }

    /**
     * @covers J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     */
    public function testIsAbsoluteRecognizesRelativePathsOnWindows()
    {
        $abs = Path::isAbsolute('a\\path', Platform::WINDOWS());
        $this->assertEquals(false, $abs);

        // Paths with one leading backslash are relative to the root of the
        // current drive (which also is the main drive in most cases, btw). Even
        // though they are quite absolute compared to 'a/path' we treat them as
        // relative.
        $abs = Path::isAbsolute('\\path', Platform::WINDOWS());
        $this->assertEquals(false, $abs);
    }

    /**
     * @covers J5lx\Path\Path::getRoot
     * @uses J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     */
    public function testGetRootReturnsEmptyPathOnUnix()
    {
        $root = Path::getRoot('/a/path', PLATFORM::UNIX());
        $this->assertEquals('', $root);
    }

    /**
     * @covers J5lx\Path\Path::getRoot
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testGetRootDetectsWindowsLocalPath()
    {
        $root = Path::getRoot('C:\\a\\path', PLATFORM::WINDOWS());
        $this->assertEquals('C:', $root);
    }

    /**
     * @covers J5lx\Path\Path::getRoot
     * @uses J5lx\Path\Path::canonical
     * @uses J5lx\Path\Path::split
     * @uses J5lx\Path\Path::join
     * @uses J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     * @uses J5lx\Path\Path::getSeparator
     */
    public function testGetRootDetectsWindowsNetworkPaths()
    {
        $root = Path::getRoot('\\\\server\\share\\a\\path', PLATFORM::WINDOWS());
        $this->assertEquals('\\\\server\\share', $root);
        $root = Path::getRoot('\\\\?\\C:\\a\\path', PLATFORM::WINDOWS());
        $this->assertEquals('\\\\?\\C:', $root);
        $root = Path::getRoot('\\\\?\\server\\share\\a\\path', PLATFORM::WINDOWS());
        $this->assertEquals('\\\\?\\server\\share', $root);
        $root = Path::getRoot('\\\\?\\UNC\\server\\share\\a\\path', PLATFORM::WINDOWS());
        $this->assertEquals('\\\\?\\UNC\\server\\share', $root);
    }

    /**
     * @covers J5lx\Path\Path::getRoot
     * @uses J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage $path must be absolute.
     */
    public function testGetRootThrowsExceptionWhenPathIsRelativeOnUnix()
    {
        Path::getRoot('a\\path', PLATFORM::UNIX());
    }

    /**
     * @covers J5lx\Path\Path::getRoot
     * @uses J5lx\Path\Path::isAbsolute
     * @uses J5lx\Path\Path::getPlatform
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage $path must be absolute.
     */
    public function testGetRootThrowsExceptionWhenPathIsRelativeOnWindows()
    {
        Path::getRoot('a\\path', PLATFORM::WINDOWS());
    }

    /**
     * @covers J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::getPlatform
     */
    public function testGetSeparatorReturnsSlashOnUnix()
    {
        $separator = Path::getSeparator(Platform::UNIX());
        $this->assertEquals('/', $separator);
    }

    /**
     * @covers J5lx\Path\Path::getSeparator
     * @uses J5lx\Path\Path::getPlatform
     */
    public function testGetSeparatorReturnsBackslashOnWindows()
    {
        $separator = Path::getSeparator(Platform::WINDOWS());
        $this->assertEquals('\\', $separator);
    }

    /**
     * @covers J5lx\Path\Path::getPlatform
     */
    public function testGetPlatformReturnsGivenPlatformIfNotNull()
    {
        $platform = Path::getPlatform(Platform::WINDOWS());
        $this->assertEquals(Platform::WINDOWS, $platform);

        $platform = Path::getPlatform(Platform::UNIX());
        $this->assertEquals(Platform::UNIX, $platform);
    }

    /**
     * @covers J5lx\Path\Path::getPlatform
     */
    public function testGetPlatformReturnsWindowsIfPhpWindowsVersionMajorIsDefined()
    {
        if (!$this->canDeleteConstants()) {
            return;
        }

        defined('PHP_WINDOWS_VERSION_MAJOR') or define('PHP_WINDOWS_VERSION_MAJOR', null);
        $platform = Path::getPlatform();
        $this->assertEquals(Platform::WINDOWS, $platform);
    }

    /**
     * @covers J5lx\Path\Path::getPlatform
     */
    public function testGetPlatformReturnsUnixIfPhpWindowsVersionMajorIsNotDefined()
    {
        if (!$this->canDeleteConstants()) {
            return;
        }

        $this->deleteConstant('PHP_WINDOWS_VERSION_MAJOR');
        $platform = Path::getPlatform();
        $this->assertEquals(Platform::UNIX, $platform);
    }
}
